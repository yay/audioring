require([], function () {

try {
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    var audioContext = new AudioContext();
} catch (error) {
    console.error('Web Audio API is not supported in this browser.');
}

let audioEl = document.getElementById('audio');
let mediaElementSource = audioContext.createMediaElementSource(audioEl);
let volumeControl = audioContext.createGain();
let analyser = audioContext.createAnalyser();
let voiceAnalyser = audioContext.createAnalyser();

mediaElementSource.connect(analyser);
analyser.connect(volumeControl);
volumeControl.connect(audioContext.destination);

volumeControl.gain.value = 0.5;
analyser.fftSize = 32 * 4;


navigator.mediaDevices.getUserMedia({audio: true})
    .then(onGetMicInput)
    .catch(onGetMicInputError);

function onGetMicInput(stream) {
    let mediaStreamSource = audioContext.createMediaStreamSource(stream);
    mediaStreamSource.connect(voiceAnalyser);
    voiceAnalyser.fftSize = 128 * 4;
}

function onGetMicInputError(error) {
    console.error('The following error occurred:', error);
}


let volumeSlider = document.getElementById('volume-slider');
let playbackToggle = document.getElementById('playback-toggle');
let alphaSlider = document.getElementById('alpha-slider');
let barWidthSlider = document.getElementById('bar-width-slider');

volumeSlider.addEventListener('change', onVolumeChange, false);
playbackToggle.addEventListener('click', togglePlayback, false);
alphaSlider.addEventListener('change', onAlphaChange, false);
barWidthSlider.addEventListener('change', onBarWidthChange, false);

function togglePlayback() {
    let glyph = this.querySelector('.glyph');
    audioEl.paused ? audioEl.play() : audioEl.pause();
    glyph.className = 'glyph ' + (audioEl.paused ? 'play' : 'stop');
}

function onVolumeChange(event) {
    volumeControl.gain.value = event.target.valueAsNumber / 100;
}

function onAlphaChange(event) {
    bgAlpha = event.target.valueAsNumber / 100;
}

function onBarWidthChange(event) {
    barWidth = event.target.valueAsNumber;
}

let canvas = document.querySelector('#canvas');
let ctx = canvas.getContext('2d');
let hueOffset = 0;
let bgAlpha = 1;
let barWidth = 3;
let fpsCounter = createFpsCounter(ctx);

let logo = new Image();
logo.src = 'logo.png';

function onAnimationFrame(timestamp) {
    render(timestamp);
    window.requestAnimationFrame(onAnimationFrame);
}
window.requestAnimationFrame(onAnimationFrame);

function createPeriodicFunction(min, max) {
    let d = max - min;
    if (d === 0)
        throw `${arguments.callee.name}: 'max' should be greater than 'min'.`;
    let i = 1 / d;
    return (n) => (~~(n * i) % 2 === 0) ? (min + n % d) : (max - n % d);
}

function createPeriodicValueGenerator(min, max) {
    let counter = 0;
    let fn = createPeriodicFunction(min, max);
    return () => fn(counter++);
}

let getPenRadius = createPeriodicValueGenerator(-100, 100);

function render(timestamp) {
    ctx.fillStyle = 'rgba(0,0,0,' + bgAlpha + ')';
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    if (!audioEl.paused) {
        hueOffset <= -360 ? hueOffset = 0 : hueOffset -= 1;
        drawMusic(ctx);
        drawSpirograph(ctx, 220, 40, getPenRadius());
        if (logo.naturalWidth) { // if logo has loaded
            drawLogo(ctx, logo, -hueOffset);
        }
    }
    drawVoice(ctx);
    fpsCounter();
}

function createFpsCounter(ctx) {
    let count = 0;
    let time = 0;
    let fps = 0;

    function drawFps(fps) {
        ctx.save();
        ctx.font = '28px Dosis';
        ctx.textBaseline = 'top';
        ctx.fillStyle = 'lime';
        ctx.fillText(fps.toString(), 5, 0);
        ctx.restore();
    }
    return function fpsCounter() {
        let now = Date.now();
        if (now - time > 1000) {
            time = now;
            fps = count;
            count = 0;
        } else {
            count += 1;
        }
        drawFps(fps);
    }
}

function drawMusic(ctx) {
    let n = analyser.frequencyBinCount,
        arr = new Uint8Array(n),
        step = 360 / n;

    analyser.getByteTimeDomainData(arr);

    for (let i = 0; i < n; i++) {
        ctx.save();
        ctx.translate(canvas.width / 2, canvas.height / 2);
        ctx.rotate(toRad(step * i));
        ctx.fillStyle = 'hsl(' + ((step * i) + hueOffset) + ',100%,50%)';
        ctx.fillRect(0, 0, barWidth, arr[i]);
        ctx.restore();
    }
}

// Renders logo at the top-right corner of the canvas, rotated by the given angle.
function drawLogo(ctx, img, angle) {
    let margin = 10;
    let left = ctx.canvas.width - img.width - margin;
    let top = margin;
    let dx = img.width / 2;
    let dy = img.height / 2;

    ctx.save();
    ctx.translate(left + dx, top + dy);
    ctx.scale(Math.sin(toRad(angle)), 1);
    ctx.drawImage(img, -dx, -dy);
    ctx.restore();
}

function drawVoice(ctx) {
    let n = voiceAnalyser.frequencyBinCount,
        arr = new Uint8Array(n),
        step = canvas.width / n,
        height = canvas.height,
        offsetX = step / 2,
        value;

    voiceAnalyser.getByteFrequencyData(arr);

    ctx.save();
    for (let i = 0; i < n; i++) {
        ctx.fillStyle = 'red';
        value = Math.log(arr[i]) * 25;
        ctx.fillRect(offsetX + step * i, height - value, 3, value);
    }
    ctx.restore();
}

function drawSpirograph(ctx, R, r, pr, steps = 180, color = 'violet') {
    let canvas = ctx.canvas;
    let step = 2 * Math.PI / steps;
    let x0 = R - r + pr;
    let y0 = 0;
    let theta = step;

    ctx.save();
    ctx.lineWidth = 2;
    ctx.globalAlpha = 0.5;
    ctx.translate(.5 * canvas.width, .5 * canvas.height);
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(x0, y0);

    while (!(x === x0 || y === y0)) {
        var x = (R - r) * Math.cos(theta) + pr * Math.cos((R - r) / r * theta);
        var y = (R - r) * Math.sin(theta) + pr * Math.sin((R - r) / r * theta);
        ctx.lineTo(x, y);
        theta += step;
    }
    ctx.stroke();
    ctx.restore();
}

// Angle of rotation depends on the distance from the center of the image.
function twirl(ctx) {
    let cw = ctx.canvas.width;
    let ch = ctx.canvas.height;
    let imgData = ctx.getImageData(0,0,cw,ch);
    let newData = ctx.getImageData(0,0,cw,ch);
    let w = imgData.width;
    let h = imgData.height;
    let amount = 5;

    for (let y = 0; y < h; y++) {
        for (let x = 0; x < w; x++) {
            let sx = x - w/2;
            let sy = y - h/2;
            let dist = Math.sqrt(sx*sx + sy*sy);
            let angle = Math.atan2(sy, sx) + dist / w * amount;
            sx = Math.cos(angle) * dist;
            sy = Math.sin(angle) * dist;
            sx = Math.floor(sx + w/2);
            sy = Math.floor(sy + h/2);
            sx = Math.min(w-1, Math.max(0, sx));
            sy = Math.min(h-1, Math.max(0, sy));
            let idx1 = (x + y*w) * 4;
            let idx2 = (sx + sy*w) * 4;
            newData.data[idx1] = imgData.data[idx2];
            newData.data[idx1+1] = imgData.data[idx2+1];
            newData.data[idx1+2] = imgData.data[idx2+2];
        }
    }
    ctx.putImageData(newData, 0, 0);
}

function toRad(degrees) {
    return degrees * Math.PI / 180;
}

});